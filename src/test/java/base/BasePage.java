package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

import static core.Log.log;

public class BasePage {
    final String TAG = "    BasePage():                   | "; // 35 + |
    private int DEFAULT_WAIT_TIME = 10; //default look for elements
    private int FAST_WAIT_TIME = 5; //default look for elements

    public WebDriver driver;

    public BasePage (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, DEFAULT_WAIT_TIME), this);
    }

    public void setDefaultWaitTime(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, DEFAULT_WAIT_TIME), this);
    }

    public void setFastWaitTime(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, FAST_WAIT_TIME), this);
    }

    // Init. elements and set specific time to wait for element
    public  void setWaitSeconds(int sec ){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, sec), this);
    }

    protected void click(WebElement element) {
        element.click();
        sleep(200);
    }

    protected void submit(WebElement element) {
        element.submit();
        sleep(200);
    }

    protected void clickBackButton() {
        driver.navigate().back();
        sleep(200);
    }

    protected void sendKeys(WebElement element, String text) {
        element.sendKeys(text);
    }

    protected void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (Exception e) {
            // ignore
        }
    }
}
