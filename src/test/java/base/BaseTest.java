package base;

import core.Website;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.lang.reflect.Method;

import static core.Log.log;

public class BaseTest {

    public WebDriver driver;
    public SoftAssert softAssert;
    public Website website;

    @BeforeMethod
    public void openBrowser() {
        log("      beforeMethod():");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");

        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver(options);
        website = new Website(driver);
        driver.get("http://www.ss.com/en");
        //driver.manage().window().fullscreen();
    }

    @AfterMethod
    public void setQuitDriver(ITestResult result, ITestContext context, Method m) {
        log("      afterMethod():");
        float runtime = (result.getEndMillis() - result.getStartMillis())/1000f; // time of test run
        log("EXECUTION TIME : "+runtime +" sec.");
        if (!result.isSuccess()){
            takeScreenShot(m);  // take screenshot if testResult != SUCCESS
            log("TEST: '"+m.getName()+"' FAILED");
        } else {
            log("TEST: '"+m.getName()+"' PASSED");
        }
        if (driver != null){
            log("      quitDriver():");
            driver.quit();
        }
    }

    private void takeScreenShot(Method method){
        try {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            String name = (method.getName().concat(".jpg"));
            FileUtils.copyFile(scrFile, new File("target/screenshots/"+name));
        } catch (Exception e) {
            log(        "takeScreenShot(): FAILED");
        }

    }
}
