package core;

import org.testng.ITestContext;

import java.time.LocalTime;

public class Log {

    private static LocalTime getCurrentTime() {
        return java.time.LocalTime.now();
    }

    public static <T> void log(T message){
        System.out.println("["+getCurrentTime()+"]"+"  "+message.toString());

    }

    public static void logResults(ITestContext context){

        log(" RESULTS:_____________________________");
        log(" TEST: " + context.getName());
        log(" ______PASSED test: "+context.getPassedTests().size());
        log(" ______SKIPPED test: "+context.getSkippedTests().size());
        log(" ______FAILED test: "+context.getFailedTests().size());
    }
}
