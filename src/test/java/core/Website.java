package core;

import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.MemoPage;
import pages.PhonePage;

public class Website {

    WebDriver driver;

    public Website (WebDriver driver){
        this.driver = driver;
    }

    public HomePage homePage(){
        return new HomePage(driver);
    }

    public PhonePage phonePage(){
        return new PhonePage(driver);
    }

    public MemoPage memoPage(){
        return new MemoPage(driver);
    }
}
