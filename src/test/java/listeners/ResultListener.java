package listeners;

import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import static core.Log.*;

public class ResultListener implements IAnnotationTransformer, ITestListener, IInvokedMethodListener{
    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {

    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void onTestStart(ITestResult result) {
        log("+++++++++++++ START ++++++++++++++");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log("+++++++++++++ FINISH ++++++++++++++");
    }

    @Override
    public void onTestFailure(ITestResult result) {

    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {
        logResults(context);
        log("Test duration: "+ ((context.getEndDate().getTime() - context.getStartDate().getTime())/1000) + " Sec." );
    }
}
