package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static core.Log.log;

public class HomePage extends BasePage {
    final String TAG = "    HomePage():                 | "; // 35 + |

    @FindBy(css = "#mtd_14025")
    private WebElement phones;

    @FindBy(css = "#ahc_26729")
    private WebElement apple;

    @FindBy(linkText = "iPhone SE")
    private WebElement iPhone;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage checkIfPageCorrect() {
        setDefaultWaitTime();
        log(TAG+"checkIfPageCorrect(): ");
        Assert.assertEquals("Submit the announcement", "Submit the announcement");
        Assert.assertEquals("My messages", "My messages");
        Assert.assertEquals("Search", "Search");
        Assert.assertEquals("Memo", "Memo");
        return this;
    }

    public PhonePage navigateToPhoneSection() {
        log(TAG+"navigateToPhoneSection(): ");
        log(TAG+"selectPhone(): ");
        click(phones);
        click(apple);
        click(iPhone);
        return new PhonePage(driver);
    }


}
