package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static core.Log.log;

public class MemoPage extends BasePage {
    final String TAG = "    MemoPage():                 | "; // 35 + |

    public MemoPage(WebDriver driver) {
        super(driver);
    }

    public MemoPage checkIfElementIsAdded() {
        log(TAG + "checkIfElementIsAdded(): ");
        Assert.assertEquals("Favorites", "Favorites");
        Assert.assertEquals("Recently viewed ads", "Recently viewed ads");
        return this;
    }



}
