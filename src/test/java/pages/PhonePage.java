package pages;

import base.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static core.Log.log;

public class PhonePage extends BasePage {
    final String TAG = "    PhonePage():                | "; // 35 + |

    @FindBy(css = "#a_fav")
    private WebElement addToFavourites;

    @FindBy(css = "#alert_head_right")
    private WebElement popUpClose;

    @FindBy(xpath = "//*[@id=\"main_table\"]/span[2]/span/b/a")
    private WebElement memo;

    public PhonePage(WebDriver driver) {
        super(driver);
    }

    public PhonePage checkIfPageCorrect() {
        setDefaultWaitTime();
        log(TAG+"checkIfPageCorrect(): ");
        Assert.assertEquals("Mark", "Mark");
        Assert.assertEquals("Volume, Gb", "Volume, Gb");
        Assert.assertEquals("Condition", "Condition");
        return this;
    }

    public PhonePage addPhoneToMemo() {
        log(TAG+"addPhoneToMemo(): ");
        JavascriptExecutor jsx = (JavascriptExecutor)driver;
        jsx.executeScript("window.scrollBy(0,450)", "");
        click(addToFavourites);
        setDefaultWaitTime();
        return this;
    }

    public PhonePage checkIfPhoneAdded() {
        log(TAG+"checkIfPhoneAdded(): ");
        Assert.assertEquals("Attention", "Attention");
        Assert.assertEquals("Advertisement added to favorites.", "Advertisement added to favorites.");
        return this;
    }

    public MemoPage goToMemoSection() {
        log(TAG + "goToMemoSection(): ");
        click(memo);
        log(TAG+"checkIfPageCorrect(): ");
        Assert.assertEquals("Mark", "Mark");
        Assert.assertEquals("Volume, Gb", "Volume, Gb");
        Assert.assertEquals("Condition", "Condition");
        return new MemoPage(driver);
    }

}
