package tests;

import base.BaseTest;
import org.testng.annotations.Test;

public class memoTest extends BaseTest {

    @Test(description = "Open website, select a phone and add id to Memo")
    public void addElementToMemo() {
        website.homePage()
                .checkIfPageCorrect()
                .navigateToPhoneSection();
        website.phonePage()
                .checkIfPageCorrect()
                .addPhoneToMemo()
                .checkIfPhoneAdded()
                .goToMemoSection();
        website.memoPage()
                .checkIfElementIsAdded();

    }

}
